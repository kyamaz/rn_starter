import {createStore, combineReducers, applyMiddleware, compose } from 'redux'
import {vidReducer} from './../store/reducers/vid.reducer'
import Thunk from 'redux-thunk'
const composeEnhancers=window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'] || compose
const rootReducer=combineReducers({
    vid:vidReducer
})
const configureStore=()=>{
    return createStore(rootReducer, composeEnhancers(applyMiddleware(Thunk)))
}
export default configureStore