import React from 'react'
import { StyleSheet, Text, View, TouchableHighlight, TouchableNativeFeedback, Platform} from 'react-native';

 buttonMaterial =props=>{
     const content= <View style={[styles.button, {backgroundColor:props.color}, props.disabled? styles.disabled:null]}>
                <Text  style={styles.text}>
                    {props.children}
                </Text>
            </View>
    if(props.disabled){
        return content
    }
    if( Platform.OS==='android'){
        return(
            <TouchableNativeFeedback onPress={props.onPress}>
                {content}
            </TouchableNativeFeedback>
        )
    }
    return (
        <TouchableHighlight onPress={props.onPress}>
            {content}
     </TouchableHighlight>
    )
 }


 
 const styles=StyleSheet.create({
    button:{
        padding:10,
        margin:5,
        borderRadius:3

    },
    text:{
        color:"white",
        textAlign:"center"

    },
    disabled:{
        backgroundColor:"#eee"
    }
})
export {buttonMaterial as ButtonMaterial}