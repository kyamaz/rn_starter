import {Navigation} from 'react-native-navigation'
//screens
import {VidScreen} from './screens/Vid/Vid';
import {HomeScreen} from './screens/Home/Home';
import {StartTabs} from './screens/MainTabs/MainTabs';
import {MapScreen} from './screens/Map/Map';
import {CameraScreen} from './screens/Camera/Camera'
import {SideDrawerSrcreen} from './screens/Side-Drawer/Side-Drawer'


//icons 
import Icon from "react-native-vector-icons/Ionicons";

//screens registrations
Navigation.registerComponent('rnStart.StartTabs', ()=>StartTabs)
Navigation.registerComponent("rnStart.HomeScreen", ()=>HomeScreen)
Navigation.registerComponent("rnStart.VidScreen", ()=>VidScreen)
Navigation.registerComponent("rnStart.MapScreen", ()=>MapScreen)
Navigation.registerComponent("rnStart.CameraScreen", ()=>CameraScreen)
Navigation.registerComponent("rnStart.SideDrawerSrcreen", ()=>SideDrawerSrcreen)


Navigation.startSingleScreenApp({
  screen:{
    screen:"rnStart.HomeScreen",
    title:"home"
  }
})
