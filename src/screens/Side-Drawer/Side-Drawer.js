
import React, {Component} from "react";
import {  StyleSheet, Button , Dimensions } from "react-native";
import { createAnimatableComponent, View, Text } from 'react-native-animatable';
import Icon from "react-native-vector-icons/Ionicons";

import {StartTabs} from './../MainTabs/MainTabs';
import * as Animatable from 'react-native-animatable';

class sideDrawer extends React.Component {
    render() {
      return (
        // need to specify width for android 
        <View style={[{width:Dimensions.get('window').width* 0.5}, styles.container]}>
            <Animatable.Text animation="bounce" iterationCount={5} direction="alternate" >
              du dantesque Lasciate ogne speranza, voi ch'intrate 
            </Animatable.Text>

          </View>
      );
    }
  }

  const styles=StyleSheet.create({
    container:{
      paddingTop:15,
      backgroundColor:"white",
      flex:1
    }

  })
  export {sideDrawer as SideDrawerSrcreen}