
import React, {Component} from "react";
import {  StyleSheet, Button , TouchableWithoutFeedback } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import {StartTabs} from './../MainTabs/MainTabs';
import * as Animatable from 'react-native-animatable';
import { createAnimatableComponent, View, Text } from 'react-native-animatable';

class home extends React.Component {
  goHandler=()=>{
    StartTabs();
  }
    render() {
      return (
        <View style={styles.container}>
            <Animatable.Text animation="bounce" iterationCount={5} direction="alternate" >
              du dantesque Lasciate ogne speranza, voi ch'intrate 
            </Animatable.Text>
 
            <Icon.Button name="logo-twitch" 
              onPress={this.goHandler}>
               <Text style={{color:"white"}}>hyacinthum pill</Text> 
              </Icon.Button>
          </View>
      );
    }
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });

  export { home as HomeScreen };