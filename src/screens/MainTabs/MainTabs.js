import {Navigation} from 'react-native-navigation';
import {MapScreen} from './../Map/Map';
import {VidScreen} from './../Vid/Vid';
import {CameraScreen} from './../Camera/Camera'

import Icon from "react-native-vector-icons/Ionicons";

const startTabs=()=>{
    Promise.all([
        Icon.getImageSource('md-ionic', 30),
        Icon.getImageSource('md-egg', 30),
        Icon.getImageSource('md-basketball', 30),
        Icon.getImageSource('md-menu', 30),


    ]).then(
      icons=>{
        Navigation.startTabBasedApp({
          tabs:[{
              screen:"rnStart.MapScreen",
              title:"home",
              label:"home",
              icon:icons[0],
              navigatorButtons:{
                leftButtons:[
                  {
                    icon:icons[3],
                    title:"menu",
                    id:"sideMenuToggle"
                  }
                ]
              }
            },
            {
              screen:"rnStart.VidScreen",
              title:"vid",
              label:"vid",
              icon:icons[1],
              navigatorButtons:{
                leftButtons:[
                  {
                    icon:icons[3],
                    title:"menu",
                    id:"sideMenuToggle"
                  }
                ]
              }
            },
            {
              screen:"rnStart.CameraScreen",
              title:"camera",
              label:"camera",
              icon:icons[2],
              navigatorButtons:{
                leftButtons:[
                  {
                    icon:icons[3],
                    title:"menu",
                    id:"sideMenuToggle"
                  }
                ]
              }
            }
          ],
          // to style tab, tabsStyle && appStyle(android)
          tabsStyle:{
            tabBarSelectedButtonColor:"orange"
          },
          appStyle:{
            tabBarSelectedButtonColor:"orange"
          },
          drawer:{
            left:{
              screen:"rnStart.SideDrawerSrcreen"
            }
          }
        })
      }
    )  
  }

export {startTabs as StartTabs}

  
