
import React, {Component} from 'react'
import { StyleSheet, View, Text } from 'react-native';
import Mapbox from '@mapbox/react-native-mapbox-gl';
Mapbox.setAccessToken('pk.eyJ1Ijoia3lhbWF6IiwiYSI6ImNqaWZ0OTIxejBxM3YzcG9jdXY2aHBuamMifQ.TRDgsbcrGI_HPlf3LAIKxg');

class map extends React.Component {
  constructor(props){
    super(props)
    this.props.navigator.setOnNavigatorEvent(this.OnNavigatorEvent)

  }
  OnNavigatorEvent=(event)=>{
    if(event.type === "NavBarButtonPress" ){
      if(event.id==="sideMenuToggle"){
        this.props.navigator.toggleDrawer({
          side:"left"
        })
      }
    }
    //navigato life cycle
    switch(event.id) {
      case 'willAppear':
       break;
      case 'didAppear':
        break;
      case 'willDisappear':
        break;
      case 'didDisappear':
        break;
      case 'willCommitPreview':
        break;
    }
  }
  
  render() {
    return (
      <View style={styles.container}>
        <Mapbox.MapView
              styleURL={Mapbox.StyleURL.Street}
              zoomLevel={10}
              centerCoordinate={[-73.935242,  40.730610 ]}
              style={styles.container}>
          </Mapbox.MapView> 
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
export { map as MapScreen };